import os
import subprocess
import tempfile


def create_temp_files(suffix: str) -> tuple:
    """
    Creates temporary files for storing input, code, and output.

    Args:
        suffix (str): The file extension for the code file.

    Returns:
        tuple: Contains file descriptors and file names for input, code, and output files.
    """
    dir_path = './api/nucleus_backlog'
    input_fd, input_file = tempfile.mkstemp(dir=dir_path)
    code_fd, code_file = tempfile.mkstemp(dir=dir_path, suffix=suffix)
    output_fd, output_file = tempfile.mkstemp(dir=dir_path)

    return input_fd, input_file, code_fd, code_file, output_fd, output_file


def run_code(language: str, code: str, input_string: str) -> str:
    """
    Executes code in a specified programming language with given input and returns the output.

    Args:
        language (str): The programming language of the code.
        code (str): The source code to execute.
        input_string (str): The input string to pass to the program.

    Returns:
        str: The output from executing the code, or an error message.
    """
    language_suffix = {
        'Python': '.py',
        'Java': '.java',
        'C++': '.cpp',
        'C': '.c',
        'Go': '.go',
        'Rust': '.rs'
    }.get(language, None)

    if language_suffix is None:
        return "Unsupported language."

    _, input_file, _, code_file, _, output_file = create_temp_files(language_suffix)
    try:
        with open(input_file, 'w') as f_input, open(code_file, 'w') as f_code:
            f_input.write(input_string)
            f_code.write(code)

        if language == 'Python':
            command = f'python3 {code_file}'
        elif language == 'Java':
            command = f'javac {code_file} && java -cp {os.path.dirname(code_file)} {os.path.basename(code_file).replace(".java", "")}'
        elif language == 'C==':
            executable = f'{code_file}.out'
            command = f'g++ {code_file} -o {executable} && {executable}'
        elif language == 'C':
            executable = f'{code_file}.out'
            command = f'gcc {code_file} -o {executable} && {executable}'
        elif language == 'Go':
            command = f'go run {code_file}'
        elif language == 'Rust':
            executable = code_file.replace('.rs', '')
            command = f'rustc {code_file} -o {executable} && {executable}'

        result = subprocess.run(command, shell=True, input=input_string, text=True,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=15)
        output = result.stdout if result.returncode == 0 else result.stderr

    except subprocess.TimeoutExpired:
        output = "Error: Execution time limit exceeded (15s)."
    finally:
        os.remove(input_file)
        os.remove(code_file)
        os.remove(output_file)

    return output


def execute_code(language: str, code: str, input_string: str = '') -> str:
    """
    Wrapper function to execute code using the `run_code` function.
    Directly forwards the call to `run_code`, making the `language_executors` mapping unnecessary.

    Args:
        language (str): The programming language of the code.
        code (str): The source code to execute.
        input_string (str): Optional input data for the execution.

    Returns:
        str: The execution output or an error message.
    """
    return run_code(language, code, input_string)
