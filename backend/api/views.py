from uuid import uuid4

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .execute import execute_code
from .models import Room
from .serializer import CodeExecutionSerializer


@api_view(['POST'])
def compile_code(request) -> Response:
    """
    Compiles and executes the provided code in the specified language and returns the output or errors.

    Args:
        request (HttpRequest): The request object containing the code, language, and optional input data.

    Returns:
        Response: A Django Rest Framework Response object containing either the compilation output or an error message.
    """
    serializer = CodeExecutionSerializer(data=request.data)
    if serializer.is_valid():
        language = serializer.validated_data['language']
        code = serializer.validated_data['code']
        input_string = serializer.validated_data.get('input', '')

        output = execute_code(language=language, code=code, input_string=input_string)
        if output is not None:
            return Response({'output': output}, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Unsupported language.'}, status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def create_room(request) -> Response:
    """
    Creates a new collaboration room.

    :param request: Django's HttpRequest object.
    :return: A Django Rest Framework Response object with the new room details.
    """
    room_name = request.data.get('roomName')
    admin = request.data.get('admin')

    if not room_name or not admin:
        return Response({'error': 'Missing room name or admin.'}, status=status.HTTP_400_BAD_REQUEST)

    room = Room.objects.create(
        room_id=uuid4(),
        name=room_name,
        creator=admin,
        joinees=admin
    )

    response = {'room_id': str(room.room_id), 'name': room.name, 'creator': room.creator}
    return Response(response, status=status.HTTP_201_CREATED)


@api_view(['GET', 'POST'])
def join_room(request) -> Response:
    """
    Checks if room is available with given ID.
    Returns Room details.
    :param request: WSGIRequest
    :return: HttpResponse
    """
    data = request.data
    if 'room_id' and 'name' in data:
        room_id = data['roomID']
        name = data['name']
        room_detail = Room.objects.filter(room_id=room_id)
        if room_detail:
            room_detail = room_detail.get()
            users = str(room_detail.joinees) + f",{name}"
            Room.objects.filter(room_id=room_id).update(joinees=users)
            response = {'room_id': room_detail.room_id, 'name': room_detail.name, 'creator': room_detail.creator,
                        'joinees': list(users.split(","))}
            return Response(status=status.HTTP_200_OK, data=response)
        return Response(status=status.HTTP_204_NO_CONTENT, data={"error": "Room not available."})
    return Response(status=status.HTTP_417_EXPECTATION_FAILED, data=data)


@api_view()
def check(request):
    """
    Server status check.
    """
    return Response(status=status.HTTP_200_OK)
