from django.urls import include, path

from . import views

urlpatterns = [
    path('execute/', views.compile_code),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('room/create/', views.create_room),
    path('room/join/', views.join_room),
    path('ping/', views.check)
]
