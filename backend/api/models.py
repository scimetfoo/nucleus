from django.db import models


class Room(models.Model):
    room_id = models.CharField(max_length=255, primary_key=True)
    name = models.CharField(max_length=255)
    creator = models.CharField(max_length=255)
    joinees = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']
