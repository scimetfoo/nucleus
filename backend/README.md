# Nucleus Backend

## Introduction

Nucleus is an in-browser real-time code editing tool designed to facilitate seamless collaboration among developers.
Built with React, Redux, and Ant Design, it offers a rich user interface and supports various programming languages,
providing a dynamic environment for code development and review.

## Getting Started

Follow these instructions to get the project's backend server up and running

### Prerequisites

Ensure you have the following installed:

- Python 3.10+
- virtualenv

### Installing

1. **Create a virtual environment**

```shell
python -m venv nucleus_backend
```

2. **Activate the virtual environment**

```shell
source nucleus_backend/bin/activate
```

3 **Install dependencies**

```shell
pip install -r requirements.txt
```

4. **Prepare and run the database migrations**

```shell
python manage.py makemigrations
python manage.py migrate
```

5 **Start the server**

```shell
python3 manage.py runserver
```

## Running Tests

To run tests:

```sh
python -m unittest tests/execution_test.py
```
