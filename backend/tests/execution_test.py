import os
import unittest
from unittest.mock import patch

from api.execute import create_temp_files, run_code, execute_code

class TestCodeExecutionSystem(unittest.TestCase):
    def test_create_temp_files(self):
        _, input_file, _, code_file, _, output_file = create_temp_files('.py')
        try:
            self.assertTrue(os.path.exists(input_file))
            self.assertTrue(code_file.endswith('.py'))
            self.assertTrue(os.path.exists(output_file))
        finally:
            # Cleanup
            os.remove(input_file)
            os.remove(code_file)
            os.remove(output_file)

    def test_run_code_python(self):
        output = run_code('Python', 'print("Hello, World")', '')
        self.assertIn("Hello, World", output)

    def test_run_code_unsupported_language(self):
        output = run_code('UnsupportedLang', '', '')
        self.assertEqual(output, "Unsupported language.")

    @patch('api.execute.run_code')
    def test_execute_code(self, mock_run_code):
        mock_run_code.return_value = "Execution Output"
        output = execute_code('Python', 'print("Hello")', '')
        mock_run_code.assert_called_once_with('Python', 'print("Hello")', '')
        self.assertEqual(output, "Execution Output")


if __name__ == '__main__':
    unittest.main()
