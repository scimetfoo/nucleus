const languages = [
    {
        language: "C",
        mode: "clike"
    },
    {
        language: "C++",
        mode: "clike"
    },
    {
        language: "Go",
        mode: "go"
    },
    {
        language: "Java",
        mode: "clike"
    },
    {
        language: "Python",
        mode: "python"
    },
    {
        language: "Rust",
        mode: "rust"
    },

];


export default languages;
