import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import App from './App';

/**
 * The main entry point of the React application.
 *
 * This script mounts the main App component within a Router to enable SPA routing capabilities.
 * Global styles are also imported here to ensure they're applied throughout the application.
 */
ReactDOM.render(
    <React.StrictMode>
        <Router>
            <App/>
        </Router>
    </React.StrictMode>,
    document.getElementById('root')
);
