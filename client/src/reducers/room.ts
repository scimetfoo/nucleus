const roomInitialState = {
    isCreated: false,
    room_id: null,
    name: null,
    username: null,
    creator: null,
    active_users: [],
};

const room = (state = roomInitialState, action: any) => {
    switch (action.type) {
        case 'IS_CREATED':
            return {...state, isCreated: action.payload};
        case 'ID':
            return {...state, room_id: action.payload};
        case 'NAME':
            return {...state, name: action.payload};
        case 'USERNAME':
            return {...state, username: action.payload};
        case 'CREATOR':
            return {...state, creator: action.payload};
        case 'ACTIVE_USERS':
            return {...state, active_users: action.payload};
        case 'RESET':
            return roomInitialState;
        default:
            return state;
    }
};

export default room;
