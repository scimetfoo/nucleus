import {combineReducers} from "redux";

import program from "./program";
import editor from "./editor";
import room from "./room";

const reducers = combineReducers({
    program: program,
    editor: editor,
    room: room,
});


export default reducers;