const programInitialState = {
    input: "",
    output: "",
    language: "",
};

const program = (state = programInitialState, action: any) => {
    switch (action.type) {
        case 'INPUT':
            return {...state, input: action.payload};
        case 'OUTPUT':
            return {...state, output: action.payload};
        case 'LANGUAGE':
            return {...state, language: action.payload};
        case 'RESET':
            return programInitialState;
        default:
            return state;
    }
};

export default program;