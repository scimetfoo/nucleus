import React from "react";
import {BrowserRouter as Router, Redirect, Route, RouteProps, Switch} from "react-router-dom";
import {Provider, RootStateOrAny, useSelector} from "react-redux";
import store from "./reducers/store";
import Home from 'components/Home';
import EditorEnvironment from './components/Editor/Environment';

import "assets/css/global.css";

interface Props extends RouteProps {
    component: any;
}

/**
 * A higher-order component that wraps a route and conditionally redirects
 * based on the `roomCreated` state from Redux.
 *
 * @param {Props} props The route props including the component to render.
 * @returns A Route that conditionally renders a component or redirects.
 */
const ConditionalRoute: React.FC<Props> = (props) => {
    const roomCreated: boolean = useSelector((state: RootStateOrAny) => state.room.isCreated);
    const {component: Component, ...rest} = props;

    return (
        <Route
            {...rest}
            render={(props) =>
                roomCreated ? (
                    <Component {...props} />
                ) : (
                    <Redirect to={{pathname: '/', state: {from: props.location}}}/>
                )
            }
        />
    );
};


/**
 * The main application component setting up the provider, routes, and conditional routes.
 * It renders the main routing structure of the application.
 */
const App: React.FC = () => {
    return (
        <Provider store={store}>
            <Router>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <ConditionalRoute exact path="/room/:id" component={EditorEnvironment}/>
                </Switch>
            </Router>
        </Provider>
    );
};

export default App;
