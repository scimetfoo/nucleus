import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {Button, Form, Input, notification} from 'antd';
import {LoadingOutlined} from '@ant-design/icons';
import {roomAdmin, roomCreated, roomID, roomName} from 'actions';

/**
 * Typescript interface for defining the structure of the data received
 * from the create room API response.
 */
interface CreateRoomResponseData {
    room_id: string;
    creator: string;
    name: string;
}

/**
 * The Create component allows users to create a new room by providing a room name
 * and the creator's name. It handles form submission and interacts with the backend
 * to create a room, dispatching relevant information to the Redux store upon success.
 */
const CreateRoom = () => {
    const [processExecuting, setProcessExecuting] = useState(false);
    const dispatch = useDispatch();
    const history = useHistory();
    const [form] = Form.useForm();

    /**
     * Handles the form submission for creating a new room. Submits room and admin
     * information to the backend and navigates to the created room upon success.
     *
     * @param {any} values - The form values containing the room and admin names.
     */
    const onCreateRoom = async (values: any) => {
        setProcessExecuting(true);
        const payload = {roomName: values.room, admin: values.name};
        try {
            const response = await fetch("http://localhost:8000/api/room/create/", {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(payload),
            });

            if (!response.ok) throw new Error("Failed to create room");

            const data: CreateRoomResponseData = await response.json();
            // Dispatch actions to update the Redux store with the room information
            dispatch(roomID(data.room_id));
            dispatch(roomName(data.name));
            dispatch(roomAdmin(data.creator));
            dispatch(roomCreated(true));

            // Navigate to the newly created room
            history.push(`/room/${data.room_id}`);
        } catch (error) {
            notification.error({
                message: "Creation Failed",
                description: "Unable to create room. Please try again.",
                duration: 5,
            });
        } finally {
            setProcessExecuting(false);
        }
    };

    return (
        <Form form={form} layout="vertical" onFinish={onCreateRoom}>
            <Form.Item name='room' label="Enter Room Name"
                       rules={[{required: true, message: 'Room Name is required!'}]}>
                <Input size="large"/>
            </Form.Item>
            <Form.Item name='name' label="Enter Your Name"
                       rules={[{required: true, message: 'Your Name is required!'}]}>
                <Input size="large"/>
            </Form.Item>
            <Form.Item>
                {processExecuting ? (
                    <Button type="primary" disabled><LoadingOutlined/> Loading...</Button>
                ) : (
                    <Button type="primary" htmlType='submit' size="middle">Create Room</Button>
                )}
            </Form.Item>
        </Form>
    );
};

export default CreateRoom;
