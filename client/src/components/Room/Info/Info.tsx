import React from 'react';
import {resetStore} from 'actions';
import {Button, Card, List, Space, Typography} from 'antd';
import {UserOutlined} from '@ant-design/icons';
import {useHistory} from 'react-router-dom';
import {RootStateOrAny, useDispatch, useSelector} from 'react-redux';

interface RoomState {
    name: string;
    creator: string;
    room_id: string;
    activeUsers: string[];
}

/**
 * Component displaying detailed information about a chat room,
 * including the creator, room key, and a list of active users.
 */
const Info = () => {
    const {Paragraph, Text} = Typography;
    const dispatch = useDispatch();
    const history = useHistory();

    // Using typed useSelector for better type checking
    const room = useSelector((state: RootStateOrAny) => state.room) as RoomState;

    /**
     * Handles the action to leave the current room.
     * It dispatches a reset action to the store and navigates the user to the home page.
     */
    const onLeaveRoom = () => {
        dispatch(resetStore());
        history.push('/');
    };

    return (
        <div style={{margin: '8px'}}>
            <Card
                hoverable
                style={{width: '100%'}}
                title={`Room Name: ${room.name}`}
                extra={<Button type="primary" danger onClick={onLeaveRoom}>Leave</Button>}
            >
                <Space direction="vertical" size="large">
                    <div>
                        <Text strong>Room Information</Text>
                        <Space direction="vertical">
                            <Text>Created by: {room.creator}</Text>
                            <Text>Room Key: <Paragraph copyable>{room.room_id}</Paragraph></Text>
                        </Space>
                    </div>
                    <div>
                        <Text strong>Active Users</Text>
                        <List
                            dataSource={room.activeUsers}
                            renderItem={item => (
                                <List.Item>
                                    <UserOutlined/> {

                                    item}
                                </List.Item>
                            )}
                        />
                    </div>
                </Space>
            </Card>
        </div>
    );
};

export default Info;