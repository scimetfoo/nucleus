import React, { useState } from 'react';
import { Button, Form, Input, notification } from 'antd';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { roomAdmin, roomCreated, roomID, roomName } from 'actions';
import { LoadingOutlined } from '@ant-design/icons';

// Interface for the response data structure
interface Data {
  room_id: string;
  creator: string;
  name: string;
}

/**
 * The Join component provides a form for users to join a room by entering a room ID and their name.
 * It handles form submission, performs a POST request to join a room, and updates the Redux store with
 * the room details on successful joining. It also navigates the user to the joined room.
 */
const Join = () => {
    const [form] = Form.useForm();
    const [processExecuting, setProcessExecuting] = useState(false);
    const dispatch = useDispatch();
    const history = useHistory();

    /**
     * Handles the form submission to join a room.
     * @param e The form event containing the room ID and user name.
     */
    const onJoinRoom = async (e: { room_id: string; name: string }) => {
        setProcessExecuting(true);

        // Payload to be sent to the server
        const payload = { roomID: e.room_id, name: e.name };

        try {
            const response = await fetch("http://localhost:8000/api/room/join/", {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(payload)
            });

            if (!response.ok) {
                throw new Error(`Room ID: ${e.room_id} is incorrect.`);
            }

            const data: Data = await response.json();
            // Dispatch actions to update the Redux store with room details
            dispatch(roomID(data.room_id));
            dispatch(roomName(data.name));
            dispatch(roomAdmin(data.creator));
            dispatch(roomCreated(true));

            // Navigate to the joined room
            history.push(`/room/${data.room_id}`);
        } catch (error: any) {
            notification.error({
                message: "Joining Room Failed",
                description: error.message || "An unexpected error occurred.",
                duration: 3,
            });
        } finally {
            setProcessExecuting(false);
        }
    };

    return (
        <Form form={form} layout="vertical" onFinish={onJoinRoom}>
            <Form.Item
                name="room_id"
                label="Enter Room ID"
                rules={[{ required: true, message: 'Room ID is required!' }]}
            >
                <Input size="large" />
            </Form.Item>
            <Form.Item
                name="name"
                label="Enter Your Name"
                rules={[{ required: true, message: 'Your Name is required!' }]}
            >
                <Input size="large" />
            </Form.Item>
            <Form.Item>
                {processExecuting ? (
                    <Button type="primary" disabled>
                        <LoadingOutlined /> Loading...
                    </Button>
                ) : (
                    <Button id="join-button" type="primary" htmlType="submit" size="large">Join Room</Button>
                )}
            </Form.Item>
        </Form>
    );
};

export default Join;
