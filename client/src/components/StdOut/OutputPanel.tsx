import React, {useState} from 'react';
import {Card, Input} from 'antd';
import {RootStateOrAny, useSelector} from 'react-redux';

/**
 * Component to display the output or error (stderr) of executed code.
 * It uses tabs to switch between stdout and stderr views.
 */
const OutputPanel = () => {
    const {TextArea} = Input;
    const [activeTabKey, setActiveTabKey] = useState<TabKey>('0');

    // Selector hooks to obtain stdout and stderr from the Redux store.
    const stdout = useSelector((state: RootStateOrAny) => state.program.output);
    const stderr = useSelector((state: RootStateOrAny) => state.program.stderr);

    // Configuration for the tabs in the Card component.
    const tabList = [
        {key: '0', tab: 'stdout'},
        {key: '1', tab: 'stderr'},
    ];

    type TabKey = "0" | "1";

    const contentList: { [key in TabKey]: JSX.Element } = {
        "0": <TextArea rows={7} value={stdout} readOnly/>,
        "1": <TextArea rows={7} value={stderr} readOnly/>,
    };

    /**
     * Handles tab change events to switch between stdout and stderr views.
     * @param {string} key The key of the newly activated tab.
     */
    const onTabChange = (key: string) => {
        setActiveTabKey(key as TabKey);
    };

    return (
        <div style={{margin: '8px 12px 8px 8px'}}>
            <Card
                hoverable
                style={{width: '100%'}}
                title="Output"
                tabList={tabList}
                activeTabKey={activeTabKey}
                onTabChange={onTabChange}
            >
                {contentList[activeTabKey]}
            </Card>
        </div>
    );
};

export default OutputPanel;
