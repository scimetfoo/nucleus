import React, { useEffect, useRef } from "react";
import OverscrollPlugin from "smooth-scrollbar/plugins/overscroll";
import Scrollbar from "smooth-scrollbar";
import { Layout } from "antd";
// Importing custom components for the collaborative editor
import InputPanel from "components/StdIn";
import CodeRunner from "components/CodeRunner";
import OutputPanel from "components/StdOut";
import LanguageSelector from "components/LanguageSelector";
import Info from "components/Room/Info";
import Editor from "components/Editor/Core";
import {RootStateOrAny, useDispatch, useSelector} from "react-redux";
import {editorValue} from "../../../actions";

/**
 * Environment is a React component that serves as the main layout for a collaborative
 * code editing environment. It integrates various panels like Info, LanguageSelector,
 * StdIn, CodeRunner, and StdOut along with a main code editor area.
 */
const EditorEnvironment = () => {
    const { Content, Sider } = Layout;

    // Extracting room and editor related state from Redux store
    const username = useSelector((state: RootStateOrAny) => state.room.name);
    const id = useSelector((state: RootStateOrAny) => state.room.room_id);
    const theme = useSelector((state: RootStateOrAny) => state.editor.theme);
    const mode = useSelector((state: RootStateOrAny) => state.editor.mode);
    const dispatch = useDispatch();

    // Refs for side panel and editor area to apply custom scrollbar and initialize the editor
    const siderRef = useRef(null);
    const editorRef = useRef(null);

    // Effect hook for initializing the custom scrollbar on the side panel
    useEffect(() => {
        if (siderRef.current) {
            Scrollbar.use(OverscrollPlugin);
            Scrollbar.init(siderRef.current, {
                damping: 0.11, // Controls the scrolling friction
                plugins: {
                    overscroll: {
                        enable: true, // Enables the overscroll effect
                        effect: "bounce", // The effect when reaching the boundary (bounce/glue)
                        damping: 0.11, // Damping factor for the overscroll effect
                        maxOverscroll: 100, // Maximum overscroll distance
                    },
                },
            });
        }
    }, []);

    const updateCodeInStore = (code: string) => {
        dispatch(editorValue(code));
    };


    // Effect hook for re-initializing the code editor whenever dependencies change
    useEffect(() => {
        if (editorRef.current) {
            // Initializes the code editor within the provided DOM element
            Editor(editorRef.current, updateCodeInStore, id, username, theme, mode);
        }
    }, [id, username, theme, mode]);

    return (
        <Layout>
            {/* Side panel for diplaying room information, language selection, and other controls */}
            <Sider
                ref={siderRef}
                breakpoint="lg"
                width={350}
                style={{
                    overflow: "hidden",
                    height: "100vh",
                    position: "fixed",
                    left: 0,
                    backgroundColor: "#1c1c1c",
                }}
            >
                <Info />
                <LanguageSelector />
                <InputPanel />
                <CodeRunner />
                <OutputPanel />
            </Sider>
            {/* Main content area for the code editor */}
            <Layout style={{ marginLeft: 350, transition: "marginLeft 0.2s" }}>
                <Content
                    style={{
                        padding: "24px",
                        minHeight: "100vh",
                        backgroundColor: "#282c34",
                    }}
                >
                    <div
                        ref={editorRef}
                        style={{
                            height: "100%",
                            width: "100%",
                            backgroundColor: "white",
                        }}
                    />
                </Content>
            </Layout>
        </Layout>
    );
};

export default EditorEnvironment;
