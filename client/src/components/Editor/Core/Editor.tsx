import * as Y from 'yjs';
import CodeMirror from 'codemirror';
import { WebsocketProvider } from 'y-websocket';
import { CodemirrorBinding } from 'y-codemirror';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/material.css';
import 'codemirror/addon/comment/comment';
import 'codemirror/addon/edit/matchbrackets';
import 'codemirror/addon/hint/show-hint';
import 'codemirror/addon/scroll/simplescrollbars';
import 'codemirror/keymap/sublime';
import 'codemirror/mode/clike/clike';
import 'codemirror/mode/go/go';
import 'codemirror/mode/python/python';
import 'codemirror/mode/rust/rust';
import './editor.css';

/**
 * Initializes a collaborative code editor using CodeMirror, Yjs, and a WebSocket provider.
 *
 * @param {HTMLElement} editorContainer The DOM element that will host the CodeMirror editor.
 * @param {(code: string) => void} updateCodeInStore Function to update the code in the application's store.
 * @param {string} id Unique identifier for the collaborative session.
 * @param {string} username Name of the user to display in collaborative sessions.
 * @param {string} [theme="default"] The theme for the CodeMirror editor.
 * @param {string} [mode="javascript"] The programming language mode for the CodeMirror editor.
 * @returns An object containing references to the Yjs document, WebSocket provider,
 *          Y.Text instance, and the CodemirrorBinding instance for further manipulation.
 */
const Editor = (
  editorContainer: HTMLElement,
  updateCodeInStore: (code: string) => void,
  id: string,
  username: string,
  theme: string = 'default',
  mode: string = 'javascript'
): { provider: WebsocketProvider; ydoc: Y.Doc; yText: Y.Text; binding: CodemirrorBinding } => {
  const ydoc = new Y.Doc();
  const provider = new WebsocketProvider('ws://localhost:1234', id, ydoc);
  const yText = ydoc.getText('codemirror');

  const editor = CodeMirror(editorContainer, {
    value: '',
    theme,
    mode,
    lineNumbers: true,
    showHint: true,
    matchBrackets: true,
    scrollbarStyle: 'overlay',
    keyMap: 'sublime',
    lineWrapping: true,
  });

  const binding = new CodemirrorBinding(yText, editor, provider.awareness);

  ydoc.on('update', () => {
    const currentCode = yText.toString();
    updateCodeInStore(currentCode);
  });

  binding.awareness.setLocalStateField('user', { color: '#FFF', name: username });

  if (id) {
    provider.connect();
  } else {
    provider.disconnect();
  }

  return { provider, ydoc, yText, binding };
};

export default Editor;
