import React, { useState } from 'react';
import { Button } from 'antd';
import {RootStateOrAny, useDispatch, useSelector} from 'react-redux';
import { codeOutput } from 'actions';

/**
 * Component for executing code by sending it to a backend server for compilation and execution.
 */
const CodeRunner = () => {
    const [processExecuting, setProcessExecuting] = useState(false);
    const dispatch = useDispatch();
    // Use useSelector to directly access the relevant state parts
    const language = useSelector((state: RootStateOrAny) => state.program.language);
    const code = useSelector((state: RootStateOrAny) => state.editor.value);
    const input = useSelector((state: RootStateOrAny) => state.program.input);

    const onClick = async () => {
        setProcessExecuting(true);
        // Consolidate payload using useSelector hook values
        const payload = { language, code, input };
        try {
            const response = await fetch("http://localhost:8000/api/execute/", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            });

            const data = await response.json(); // Parse JSON response in all cases to handle both success and error responses
            if (!response.ok) {
                dispatch(codeOutput("Internal Server Error."));
                console.error(data); // Use console.error for errors
            } else {
                dispatch(codeOutput(data.output));
            }
        } catch (error) {
            console.error("Error executing code:", error);
            dispatch(codeOutput("Failed to execute code."));
        } finally {
            setProcessExecuting(false);
        }
    };

    return (
        <div style={{ margin: "8px 12px 8px 8px" }}>
            <Button
                disabled={processExecuting} // Disable the button when process is executing
                style={{ width: '100%' }}
                size='large'
                type='primary'
                onClick={onClick}
            >
                {processExecuting ? "Running..." : "Run"}
                {/* Provide user feedback when the code is being executed */}
            </Button>
        </div>
    );
};

export default CodeRunner;
