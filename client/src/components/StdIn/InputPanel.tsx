import React from 'react';
import { Card, Input } from 'antd';
import { useDispatch } from "react-redux";
import { codeInput } from "actions";

/**
 * StdIn component provides a text area for users to input text that
 * can be considered as standard input (stdin) for code execution.
 *
 * The component utilizes Ant Design's Card and TextArea components to
 * provide a UI element where users can enter input data. This input
 * data is then dispatched to the Redux store using the codeInput action
 * for later use in code execution.
 */
const InputPanel = () => {
    const { TextArea } = Input; // Destructuring TextArea from Input for direct usage
    const dispatch = useDispatch(); // Hook to access the Redux dispatch function

    return (
        <div style={{ margin: "8px 12px 8px 8px" }}>
            <Card
                hoverable
                style={{ width: '100%' }}
                title='Input (stdin)'
            >
                <TextArea
                    onChange={(e) => dispatch(codeInput(e.target.value))}
                    rows={7} // Defines the height of the text area by line count
                />
            </Card>
        </div>
    );
};

export default InputPanel;
