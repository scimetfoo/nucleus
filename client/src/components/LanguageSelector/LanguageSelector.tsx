import React from 'react';
import { Card, Select } from 'antd';
import { useDispatch } from 'react-redux';
import { codeLanguage, editorMode } from 'actions';
import modes from 'assets/ts/editor/modes';

/**
 * A component for selecting a programming language and mode for the code editor.
 *
 * This component renders a dropdown list (Select component from Ant Design) populated
 * with programming languages fetched from a predefined list (`modes`). When a language
 * is selected, it dispatches actions to update the code editor's current language and mode
 * in the global Redux store.
 */
const LanguageSelector = () => {
    const { Option } = Select;
    const dispatch = useDispatch();

    /**
     * Handles changes in the language selection dropdown.
     *
     * @param {string} value - The value of the selected option, which includes both the
     * language and the mode concatenated with a '$'.
     */
    const onChange = (value: string) => {
        const [language, mode] = value.split('$');
        dispatch(editorMode(mode));
        dispatch(codeLanguage(language));
    };

    return (
        <div style={{ margin: '8px 12px 8px 8px' }}>
            <Card
                hoverable
                style={{ width: '100%' }}
                title="Select Language"
            >
                <Select
                    showSearch
                    size="large"
                    placeholder="Select language"
                    onChange={onChange}
                    style={{ width: '100%' }}
                >
                    {modes.map((mode) => (
                        <Option key={`${mode.language}$${mode.mode}`} value={`${mode.language}$${mode.mode}`}>
                            {mode.language}
                        </Option>
                    ))}
                </Select>
            </Card>
        </div>
    );
};

export default LanguageSelector;
