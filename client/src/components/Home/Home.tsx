import React, {useEffect} from 'react';
import {Card, notification, Tabs} from "antd";
import Create from 'components/Room/Create';
import Join from 'components/Room/Join';

/**
 * The Home component serves as the landing page for the application, offering
 * users the options to either create a new room or join an existing one.
 * It performs a simple server check on mount to ensure backend connectivity.
 */
const Home = () => {
    useEffect(() => {
        fetch('http://localhost:8000/api/ping/')
            .catch(() => {
                notification.error({
                    message: "Server Unreachable",
                    description: "Unable to connect to the server. Please try again later.",
                    duration: 5,
                });
            });
    }, []);

    return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh'}}>
            <div style={{width: '600px'}}>
                <h1 style={{textAlign: 'center'}}>Nucleus<sup>beta</sup></h1>
                <h2 style={{textAlign: 'center'}}>In-Browser collaborative code editor.</h2>
                <Card>
                    <Tabs defaultActiveKey='create' size='large'>
                        <Tabs.TabPane key='create' tab='Create Room'>
                            <Create/>
                        </Tabs.TabPane>
                        <Tabs.TabPane key='join' tab='Join Room'>
                            <Join/>
                        </Tabs.TabPane>
                    </Tabs>
                </Card>
            </div>
        </div>
    );
};

export default Home;
