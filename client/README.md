# Nucleus Client

## Introduction

Nucleus is an in-browser real-time code editing tool designed to facilitate seamless collaboration among developers.
Built with React, Redux, and Ant Design, it offers a rich user interface and supports various programming languages,
providing a dynamic environment for code development and review.

## Getting Started

Follow these instructions to get the project up and running

### Prerequisites

Ensure you have the following installed:

- Node.js (version 14 or later)
- npm (version 6 or later)
- yarn

### Installing

1**Install dependencies**

```shell
yarn install
```

2. Run the websocket locally:

```shell
HOST=localhost PORT=1234 npx y-websocket
```

2**Start the development server**

```shell
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view the app in your browser.

## Running Tests

To run tests:

```sh
npm test
```
